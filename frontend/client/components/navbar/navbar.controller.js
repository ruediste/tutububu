'use strict';

angular.module('tutububuApp')
  .controller('NavbarCtrl', function ($scope, $location, $state) {
    $scope.menu = [{
      'title': 'Home',
      'link': '/'
    }];

    $scope.isCollapsed = true;

    $scope.isMainPage = function() {
        return $state.current.name === 'main';
    };

    $scope.isNotMainPage = function() {
        return $state.current.name !== 'main';
    };

    $scope.icon = 'menu';

    $scope.changeIcon = function() {
        if ($scope.icon === 'menu') {
            $scope.icon = 'close';
        } else {
            $scope.icon = 'menu';
        }
    }
  });
