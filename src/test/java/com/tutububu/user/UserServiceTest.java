package com.tutububu.user;

import java.io.IOException;

import javax.json.JsonObject;

import org.junit.Test;

import com.jcabi.http.request.JdkRequest;
import com.jcabi.http.response.JsonResponse;

public class UserServiceTest {

    @Test
    public void testScrape() throws IOException {
        String accessToken = "";

        JsonObject data = new JdkRequest("https://graph.facebook.com").uri()
                .path("/debug_token").queryParam("input_token", accessToken)
                .queryParam("access_token", accessToken).back().method("GET")
                .fetch().as(JsonResponse.class).json().readObject()
                .getJsonObject("data");
        System.out.println("usrid:" + data.getString("user_id"));
        System.out.println("appid:" + data.getString("app_id"));
        System.out.println("valid:" + data.getBoolean("is_valid"));

        JsonObject user = new JdkRequest("https://graph.facebook.com").uri()
                .path("/v2.4/me").queryParam("access_token", accessToken)
                .queryParam("fields",
                        "about,age_range,bio,birthday,context,currency,education,email,gender,political,cover,work")
                .back().method("GET").fetch().as(JsonResponse.class).json()
                .readObject();

        System.out.println(user);
        JsonObject likes = new JdkRequest("https://graph.facebook.com").uri()
                .path("/v2.4/me/likes").queryParam("access_token", accessToken)
                .back().method("GET").fetch().as(JsonResponse.class).json()
                .readObject();

        System.out.println(likes);
    }
}
