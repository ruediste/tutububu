package com.tutububu.nlp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Set;

import org.junit.Test;

import com.tutububu.nlp.IdfCalculator.Document;
import com.tutububu.nlp.IdfCalculator.TermStats;

public class IdfCalculatorTest {

    IdfCalculator calc = new IdfCalculator();

    @Test
    public void testTermStats() {
        TermStats stats = calc.getTermStats(Arrays
                .asList(new Document("hello world"), new Document("hello")));
        assertEquals(2, (int) stats.stats.get("hello"));
        assertEquals(1, (int) stats.stats.get("world"));
    }

    @Test
    public void testGetTermsFromDocument() {
        Set<String> terms = calc
                .getTermsFromDocument(new Document("hello world Hello"));
        assertTrue(terms.contains("hello"));
        assertTrue(terms.contains("world"));
        assertEquals(2, terms.size());
    }
}
