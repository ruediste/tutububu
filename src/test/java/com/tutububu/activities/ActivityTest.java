package com.tutububu.activities;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class ActivityTest {

    @Test
    public void testFields() {
        Map<String, String> list = new HashMap<>();
        list.put("a", "b");
        Activity activity = new Activity();
        activity.setProperties(list);
        assertEquals(list, activity.getProperties());
    }
}
