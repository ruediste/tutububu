'use strict';

angular.module('tutububuApp')
  .controller('HomeCtrl', function ($scope, $window, $http) {
      // var url = '/api/v1/activities';
      var url = '/api/v1/activities';
      var toThrowout = false;

      $scope.events = {};
      $scope.likes = {};
      $scope.eventsToDisplay = null;

      $http.get(url).success(function(event1) {
          $scope.events[0] = event1;
          $http.get(url).success(function(event2) {
              $scope.events[1] = event2;
              $http.get(url).success(function(event3) {
                  $scope.events[2] = event3;
                  $scope.eventsToDisplay = $scope.events;
              });
          });
      });

      $scope.dismiss = function(index) {
          console.log(index);
          $scope.onThrowout(null, null, index);
      };

      $scope.onDragend = function(eventName, eventObject) {
          if (toThrowout) {
              $scope.onThrowout(eventName, eventObject);
          }
      };

      $scope.onDragmove = function(eventName, eventObject) {
          if (eventObject.throwOutConfidence > 0.6) {
              toThrowout = true;
              eventObject.target.classList.add('almost-thrown-away');
          } else {
              toThrowout = false;
              eventObject.target.classList.remove('almost-thrown-away');
          }
      };

      $scope.onThrowout = function(eventName, eventObject, index) {
          var id, force;
          if (eventObject) {
              id = eventObject.target.id;
              force = true;
          } else {
          }
          if (document.querySelector('#' + id)) {
              console.log('Removed element');
              document.querySelector('#' + id).remove();   
          }
          if (force) {
              $scope.$apply(function() {
                  delete $scope.events[2];
                  $http.get(url).success(function(event) {
                      $scope.events[2] = $scope.events[1];
                      $scope.events[1] = $scope.events[0];
                      $scope.events[0] = event;
                      $scope.eventsToDisplay = $scope.events;
                  });
              });
          } else {
              delete $scope.events[2];
              $http.get(url).success(function(event) {
                  $scope.events[2] = $scope.events[1];
                  $scope.events[1] = $scope.events[0];
                  $scope.events[0] = event;
                  $scope.eventsToDisplay = $scope.events;
              });
          }
          console.log(eventName, eventObject, $scope.events, $scope.eventsToDisplay);
      };

      $scope.open = function(link) {
          $window.location.href = link;
      };

      $scope.like = function(url) {
          $scope.likes[url] = true;
          $scope.onThrowout(null, null, 2);
      };

      $scope.dislike = function(url) {
          $scope.likes[url] = false;
          $scope.onThrowout(null, null, 2);
      };
  })
  .filter('reverse', function() {
      return function(items) {
          return items.slice().reverse();
      };
  });
