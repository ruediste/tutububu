'use strict';

angular.module('tutububuApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('home', {
        url: '/home',
        templateUrl: 'assets/app/home/home.html',
        controller: 'HomeCtrl'
      });
  });