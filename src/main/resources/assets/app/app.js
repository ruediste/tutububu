'use strict';

angular.module('tutububuApp', [
//    'ngCookies',
//    'ngResource',
//    'ngSanitize',
//    'ngMaterial',
//    'ngMdIcons',
//    'gajus.swing',
    'ui.router'
])
.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider
    .otherwise('/');

    $locationProvider.html5Mode(true);
})
//.config(function($mdThemingProvider) {
//    $mdThemingProvider.theme('default')
//        .primaryPalette('cyan')
//        .accentPalette('teal');
//})
//.run(function() {
//    $(document).ready(function(){
//        $.adaptiveBackground.run({
//            success: function($img, data) {
//                    console.log('Success!', $img, data);
//            }
//
//        });
//    });
//})
;
