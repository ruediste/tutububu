'use strict';

angular.module('tutububuApp')
  .controller('MainCtrl', function ($scope, $http, $window) {
    $scope.randomNumbers = [];

    $scope.widthInTiles = ~~( $window.innerWidth / 100 ) + 1;
    $scope.heightInTiles = 5;
    console.log($scope.widthInTiles, $scope.heightInTiles);

    var getRandom = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };

    for (var i = 0, l = $scope.widthInTiles; i < l; i++) {
        $scope.randomNumbers.push(getRandom(1, 35 - $scope.widthInTiles));
    }

  })
  .filter('range', function() {
      return function(input, amount, start) {
          start = start ? parseInt(start) : 0;
          var total = parseInt(amount) + start;

          for (var i=start; i<total; i++) {
              input.push(i);
          }

          return input;
      };
  });
