'use strict';

angular.module('tutububuApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'assets/app/main/main.html',
        controller: 'MainCtrl'
      })
  });
