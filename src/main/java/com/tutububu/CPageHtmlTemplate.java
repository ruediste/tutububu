package com.tutububu;

import javax.inject.Inject;

import com.github.ruediste.rise.component.components.CPage;
import com.github.ruediste1.i18n.lString.LString;
import com.tutububu.TuTuBuBuPageTemplate.SamplePageTemplateParameters;

public class CPageHtmlTemplate extends ComponentTemplate<CPage> {

    @Inject
    TuTuBuBuAssetBundle bundle;

    @Inject
    TuTuBuBuPageTemplate template;

    @Override
    public void doRender(CPage component, SampleCanvas html) {
        template.renderOn(html, new SamplePageTemplateParameters() {

            @Override
            public void renderContent(SampleCanvas html) {
                html.renderChildren(component);
            }

            @Override
            public LString getTitle() {
                LString title = component.getTitle();

                return title == null ? (locale -> "") : title;
            }
        });
    }
}
