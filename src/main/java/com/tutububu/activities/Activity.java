package com.tutububu.activities;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.github.ruediste.rise.crud.annotations.CrudBrowserColumn;
import com.github.ruediste1.i18n.label.Labeled;
import com.github.ruediste1.i18n.label.PropertiesLabeled;

@Entity
@Labeled
@PropertiesLabeled
public class Activity {

    @GeneratedValue
    @Id
    private long id;

    @CrudBrowserColumn
    private String title;
    private String url;
    private String imgUrl;
    private String propertiesJSON;

    private String scraper;
    private String reference;

    public Map<String, String> getProperties() {
        LinkedHashMap<String, String> result = new LinkedHashMap<>();
        JSONArray list = (JSONArray) JSONValue.parse(getPropertiesJSON());
        for (Object element : list) {
            JSONObject obj = (JSONObject) element;
            result.put((String) obj.get("key"), (String) obj.get("value"));
        }
        return result;
    }

    public void setProperties(Map<String, String> properties) {
        JSONArray list = new JSONArray();
        for (Entry<String, String> field : properties.entrySet()) {
            JSONObject obj = new JSONObject();
            obj.put("key", field.getKey());
            obj.put("value", field.getValue());
            list.add(obj);
        }
        setPropertiesJSON(list.toString());
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPropertiesJSON() {
        return propertiesJSON;
    }

    public void setPropertiesJSON(String propertiesJSON) {
        this.propertiesJSON = propertiesJSON;
    }

    public String getScraper() {
        return scraper;
    }

    public void setScraper(String scraper) {
        this.scraper = scraper;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

}
