package com.tutububu.activities;

import java.util.List;
import java.util.function.Consumer;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class EmHelper {

    public <T> List<T> getAll(EntityManager em, Class<T> entityClass) {
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<T> cq = cb.createQuery(entityClass);
        Root<T> from = cq.from(entityClass);
        cq.select(from);
        return em.createQuery(cq).getResultList();
    }

    public static class FilterContext<T> {
        public CriteriaBuilder cb;
        public CriteriaQuery<T> cq;
        public Root<T> from;

        FilterContext(CriteriaBuilder cb, CriteriaQuery<T> cq, Root<T> from) {
            super();
            this.cb = cb;
            this.cq = cq;
            this.from = from;
        }

    }

    public <T> List<T> getFiltered(EntityManager em, Class<T> entityClass,
            Consumer<FilterContext<T>> filter) {
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<T> cq = cb.createQuery(entityClass);
        Root<T> from = cq.from(entityClass);
        cq.select(from);
        filter.accept(new FilterContext<>(cb, cq, from));

        return em.createQuery(cq).getResultList();
    }
}
