package com.tutububu.activities;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.github.ruediste.rise.nonReloadable.SignatureHelper;

import jersey.repackaged.com.google.common.collect.Iterables;

@Singleton
public class ActivityRepository {

    @Inject
    EntityManager em;

    @Inject
    SignatureHelper helper;

    @Inject
    EmHelper emHelper;

    public List<Activity> getAllActivities() {
        return emHelper.getAll(em, Activity.class);

    }

    public Activity getActivity(String scraper, String reference) {

        return Iterables
                .getFirst(emHelper.getFiltered(em, Activity.class,
                        c -> c.cq.where(
                                c.cb.equal(c.from.get(Activity_.scraper),
                                        scraper),
                                c.cb.equal(c.from.get(Activity_.reference),
                                        reference))),
                        null);
    }

    public Activity getRandomActivity() {
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<Activity> cq = cb.createQuery(Activity.class);
        Root<Activity> from = cq.from(Activity.class);
        cq.select(from);
        List<Activity> results = em.createQuery(cq).getResultList();

        Activity activity;
        if (results.isEmpty()) {
            activity = new Activity();
            Map<String, String> properties = new HashMap<>();
            properties.put("No Data", "No Data");
            activity.setProperties(properties);
            activity.setUrl("#");
        } else {
            int idx = helper.getRandom().nextInt(results.size());
            activity = results.get(idx);
        }
        return activity;
    }
}
