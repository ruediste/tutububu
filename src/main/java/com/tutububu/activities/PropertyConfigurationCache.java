package com.tutububu.activities;

import static java.util.stream.Collectors.toList;

import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;

import com.github.ruediste.rise.core.persistence.TransactionTemplate;

@Singleton
public class PropertyConfigurationCache {

    private List<PropertyConfiguration> configs;

    @Inject
    TransactionTemplate trx;

    @Inject
    EntityManager em;

    @Inject
    EmHelper helper;

    public void loadConfig() {
        trx.execute(() -> {
            configs = helper.getAll(em, PropertyConfiguration.class).stream()
                    .sorted(Comparator.comparing(c -> c.getOrdinal()))
                    .collect(toList());
        });
    }

    public List<PropertyConfiguration> getConfigs() {
        if (configs == null)
            loadConfig();
        return configs;
    }

}
