package com.tutububu;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import com.github.ruediste.rise.core.security.authorization.MetaRequiresRight;

@Retention(RetentionPolicy.RUNTIME)
@MetaRequiresRight
public @interface RequiresAdmin {

    String value() default "admin";
}
