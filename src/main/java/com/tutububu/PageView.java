package com.tutububu;

import java.util.function.Consumer;

import javax.inject.Inject;

import com.github.ruediste.rise.mvc.IControllerMvc;
import com.github.ruediste1.i18n.lString.LString;
import com.github.ruediste1.i18n.label.LabelUtil;
import com.tutububu.TuTuBuBuPageTemplate.SamplePageTemplateParameters;

public abstract class PageView<TController extends IControllerMvc, TData>
        extends ViewMvc<TController, TData>
        implements SamplePageTemplateParameters {

    @Inject
    TuTuBuBuAssetBundle bundle;

    @Inject
    TuTuBuBuPageTemplate template;

    @Inject
    LabelUtil labelUtil;

    @Override
    public LString getTitle() {
        return labelUtil.getTypeLabel(getClass());
    }

    @Override
    public final void render(SampleCanvas html) {
        template.renderOn(html, this);
    }

    public LString label(Class<?> clazz) {
        return labelUtil.getTypeLabel(clazz);
    }

    public <T> LString label(Class<T> clazz, Consumer<T> accessor) {
        return labelUtil.tryGetMethodLabel(clazz, accessor).get();
    }

    public LString label(Object obj) {
        return labelUtil.getTypeLabel(obj.getClass());
    }
}
