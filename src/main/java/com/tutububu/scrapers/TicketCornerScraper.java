package com.tutububu.scrapers;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.github.ruediste.rise.core.persistence.TransactionTemplate;
import com.github.ruediste1.i18n.label.Labeled;
import com.github.ruediste1.lambdaPegParser.Var;
import com.tutububu.activities.Activity;
import com.tutububu.activities.ActivityRepository;

@Labeled
public class TicketCornerScraper implements Scraper {

    @Inject
    org.slf4j.Logger log;

    @Inject
    TransactionTemplate trx;

    @Inject
    EntityManager em;

    @Inject
    ActivityRepository repo;

    @Override
    public void run(ExecutorService executor) {
        Document doc;
        try {
            doc = Jsoup
                    .connect(
                            "https://www.ticketcorner.ch/Tickets.html?affiliate=TCS&doc=category&fun=kategorieliste&detailadoc=erdetaila&detailbdoc=evdetailb&hkId=33&kategorieKuerzel=dance&nurbuchbar=true&showFilter=yes&tipps=yes")
                    .get();

            doc.select("#taEventList tr").forEach(tr -> {
                Elements imgs = tr.select("td.taImage img");
                if (imgs.size() == 0)
                    return;
                String imgUrl = "https://www.ticketcorner.ch/"
                        + imgs.get(0).attr("src").replace("60x60", "222x222");
                String url = "https://www.ticketcorner.ch/"
                        + tr.select("td.taEvent a").attr("href");

                Var<Activity> var = new Var<>();
                trx.execute(() -> {
                    var.setValue(
                            repo.getActivity(getClass().getSimpleName(), url));
                });
                trx.updating().execute(() -> {
                    try {
                        visitEvent(var.getValue(), url, tr, imgUrl);
                    } catch (IOException ex) {
                        throw new RuntimeException(ex);
                    }
                });
            });
        } catch (IOException e) {
            log.warn("Error", e);
        }
    }

    private void visitEvent(Activity activity, String url, Element tr,
            String imgUrl) throws IOException {

        boolean newActivity = false;
        if (activity == null) {
            activity = new Activity();
            activity.setScraper(getClass().getSimpleName());
            activity.setReference(url);
            newActivity = true;
        } else {
            activity = em.merge(activity);
            log.info("Activity already present, updating it: ", url);
        }

        activity.setTitle(tr.select("td.taEvent h4").text());
        activity.setImgUrl(imgUrl);
        activity.setUrl(url);

        Map<String, String> properties = new LinkedHashMap<>();
        properties.put("date", tr.select("td.taEvent .date").text());
        activity.setProperties(properties);

        if (newActivity)
            em.persist(activity);

    }
}
