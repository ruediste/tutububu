package com.tutububu.scrapers;

import java.util.concurrent.ExecutorService;

public interface Scraper {

    public void run(ExecutorService executor);

}
