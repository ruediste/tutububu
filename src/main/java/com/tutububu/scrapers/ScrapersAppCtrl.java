package com.tutububu.scrapers;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;

import org.slf4j.Logger;

import com.github.ruediste.c3java.properties.NoPropertyAccessor;
import com.github.ruediste.salta.jsr330.Injector;
import com.github.ruediste1.i18n.label.Label;

@Singleton
public class ScrapersAppCtrl {

    @Inject
    Logger log;

    @Inject
    Injector injector;

    private final List<Class<? extends Scraper>> availableScrapers = new ArrayList<>();

    private int nextScraperNr = 0;

    private final Map<Integer, RunningScraper> runningScrapers = new LinkedHashMap<>();

    public static class RunningScraper {
        int id;
        Scraper scraper;
        ExecutorService executor;
        public Class<? extends Scraper> scraperClass;
        Instant start = Instant.now();

        public void cancel() {
            executor.shutdown();
        }
    }

    @PostConstruct
    public void initialize() throws IOException {
        getAvailableScrapers().add(TillateScraper.class);
        getAvailableScrapers().add(TicketCornerScraper.class);
    }

    public void startScraper(Class<? extends Scraper> scraperClass) {
        log.info("starting scraper {}", scraperClass);
        Scraper scraper = injector.getInstance(scraperClass);
        ForkJoinPool executor = new ForkJoinPool(5);

        RunningScraper runningScraper = new RunningScraper();
        runningScraper.scraperClass = scraperClass;
        runningScraper.scraper = scraper;
        runningScraper.executor = executor;
        executor.submit(ThrowingRunnable.runnable(() -> {
            synchronized (runningScrapers) {
                runningScraper.id = nextScraperNr++;
                runningScrapers.put(runningScraper.id, runningScraper);
            }
            try {
                scraper.run(executor);
            } finally {
                try {
                    log.info("awaiting finish of scraper {}:{}",
                            runningScraper.id, scraperClass);
                    executor.awaitQuiescence(1, TimeUnit.DAYS);
                } finally {
                    log.info("scraper {}:{} finished", runningScraper.id,
                            scraperClass);
                    executor.shutdownNow();
                    synchronized (runningScrapers) {
                        runningScrapers.remove(runningScraper.id);
                    }
                }
            }
        }));
    }

    @Label("Available Scrapers")
    @NoPropertyAccessor
    public List<Class<? extends Scraper>> getAvailableScrapers() {
        return availableScrapers;
    }

    @Label("Running Scrapers")
    @NoPropertyAccessor
    public Map<Integer, RunningScraper> getRunningScrapers() {
        synchronized (runningScrapers) {
            return new LinkedHashMap<>(runningScrapers);
        }
    }

    public void cancel(int scraperId) {
        synchronized (runningScrapers) {
            RunningScraper runningScraper = runningScrapers.remove(scraperId);
            if (runningScraper != null) {
                log.info("cancelling scraper {}:{}", scraperId,
                        runningScraper.scraperClass);
                runningScraper.cancel();
            } else {
                log.info("scraper with id {} not found for cancelling",
                        scraperId);
            }
        }
    }
}
