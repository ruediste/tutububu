package com.tutububu.scrapers;

@FunctionalInterface
public interface ThrowingRunnable {

    void run() throws Throwable;

    static Runnable runnable(ThrowingRunnable run) {
        return () -> {
            try {
                run.run();
            } catch (Throwable t) {
                throw new RuntimeException(t);
            }
        };
    }
}
