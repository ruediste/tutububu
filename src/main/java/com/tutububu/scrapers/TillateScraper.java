package com.tutububu.scrapers;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;

import com.github.ruediste.rise.core.persistence.TransactionTemplate;
import com.github.ruediste1.i18n.label.Labeled;
import com.github.ruediste1.lambdaPegParser.Var;
import com.google.common.base.Strings;
import com.tutububu.activities.Activity;
import com.tutububu.activities.ActivityRepository;

@Labeled
public class TillateScraper implements Scraper {

    @Inject
    Logger log;

    @Inject
    EntityManager em;

    @Inject
    ActivityRepository repo;

    @Inject
    TransactionTemplate trx;

    private ExecutorService executor;

    @Override
    public void run(ExecutorService executor) {
        this.executor = executor;
        executor.submit(ThrowingRunnable.runnable(this::processTillate));
    }

    public void processTillate() throws IOException {
        Document document = Jsoup
                .connect(
                        "http://ch.tilllate.com/de/events/zentralschweiz?ref=navigation-events")
                .get();
        document.select(".events-event-row-title a").forEach(e -> {
            String link = e.attr("href");
            log.info("queuing {}", link);
            executor.submit(ThrowingRunnable.runnable(() -> {
                Var<Activity> var = new Var<>();
                String url = "http://ch.tilllate.com" + link;
                trx.execute(() -> {
                    var.setValue(
                            repo.getActivity(getClass().getSimpleName(), url));
                });
                trx.updating().execute(() -> {
                    try {
                        visitEvent(var.getValue(), url);
                    } catch (IOException ex) {
                        throw new RuntimeException(ex);
                    }
                });
            }));
        });
    }

    private void visitEvent(Activity activity, String url) throws IOException {

        boolean newActivity = false;
        if (activity == null) {
            activity = new Activity();
            activity.setScraper(getClass().getSimpleName());
            activity.setReference(url);
            newActivity = true;
        } else {
            activity = em.merge(activity);
            log.info("Activity already present, updating it: ", url);
        }

        Document document = Jsoup.connect(url).get();

        activity.setTitle(document.select("*[itemprop=summary]").text());
        activity.setImgUrl(document.select("*[itemprop=photo]").attr("src"));

        // skip events without image
        if (Strings.isNullOrEmpty(activity.getImgUrl()))
            return;

        activity.setUrl(url);

        // System.out.println(
        // document.select("*[class=event_detail_location_link]").text());

        Map<String, String> properties = new LinkedHashMap<>();
        add(properties, "location-link",
                document.select("*[class=event_detail_location_link]").text());
        add(properties, document, "name");
        add(properties, document, "startData");
        add(properties, document, "endDate");
        add(properties, document, "street-address");
        add(properties, document, "postal-code");
        add(properties, document, "description");
        add(properties, document, "performer");
        add(properties, document, "eventtype");
        add(properties, document, "locality");
        activity.setProperties(properties);
        if (newActivity)
            em.persist(activity);
    }

    private void add(Map<String, String> fields, Document document,
            String label) {
        add(fields, label, document.select("*[itemprop=" + label + "]").text());
    }

    private void add(Map<String, String> fields, String label, String text) {
        if (!Strings.isNullOrEmpty(text)) {
            fields.put(label, text);
        }
    }

}
