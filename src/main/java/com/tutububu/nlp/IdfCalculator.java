package com.tutububu.nlp;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.github.ruediste.rise.util.Pair;

public class IdfCalculator {

    public static void main(String... args) {

        new IdfCalculator().calculate();
    }

    public static class Document {
        String text;

        Document(String text) {
            this.text = text;
        }
    }

    void calculate() {
        List<Document> docs = new ArrayList<>();
        docs.add(new Document(
                "A bottle is a rigid container with a neck that is narrower than the body and a mouth. By contrast, a jar has a relatively large mouth or opening which may be as wide as the overall container.    "));
        docs.add(new Document(
                "The horse (Equus ferus caballus)[2][3] is one of two extant subspecies of Equus ferus. It is an odd-toed ungulate mammal belonging to the taxonomic family Equidae. The horse has evolved over the past 45 to 55 million years from a small multi-toed creature, Hyracotherium, into the large, single-toed animal of today. "));
        TermStats stats = getTermStats(docs);
        double totalCount = stats.totalCount;
        List<Pair<String, Double>> topTerms = stats.stats.entrySet().stream()
                .map(e -> new Pair<>(e.getKey(),
                        Math.log10(totalCount / (double) e.getValue())))
                .sorted(Comparator
                        .comparing((Pair<String, Double> e) -> e.getB())
                        .reversed())
                .limit(10).collect(toList());
        System.out.println(topTerms);
    }

    static class TermStats {
        Map<String, Integer> stats = new HashMap<>();
        int totalCount;
    }

    TermStats getTermStats(List<Document> docs) {
        TermStats result = new TermStats();
        for (Document doc : docs) {
            for (String term : getTermsFromDocument(doc)) {
                result.stats.putIfAbsent(term, 0);
                result.stats.put(term, result.stats.get(term) + 1);
                result.totalCount++;
            }
        }
        return result;
    }

    Set<String> getTermsFromDocument(Document doc) {
        HashSet<String> result = new HashSet<>();
        StringBuilder termBuilder = new StringBuilder();
        doc.text.codePoints().forEach(cp -> {
            if (Character.isAlphabetic(cp)) {
                termBuilder.appendCodePoint(cp);
            } else {
                if (termBuilder.length() > 0) {
                    String term = termBuilder.toString();
                    termBuilder.setLength(0);
                    result.add(term.toUpperCase(Locale.ENGLISH)
                            .toLowerCase(Locale.ENGLISH));
                }
            }
        });
        if (termBuilder.length() > 0) {
            String term = termBuilder.toString();
            result.add(term.toUpperCase(Locale.ENGLISH)
                    .toLowerCase(Locale.ENGLISH));
        }
        return result;
    }

}
