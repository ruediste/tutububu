package com.tutububu.welcome;

import javax.inject.Inject;

import com.github.ruediste.rendersnakeXT.canvas.Glyphicon;
import com.github.ruediste.rise.api.ControllerMvc;
import com.github.ruediste.rise.core.ActionResult;
import com.github.ruediste.rise.core.security.urlSigning.UrlUnsigned;
import com.github.ruediste.rise.core.web.ActionPath;
import com.github.ruediste1.i18n.label.Label;
import com.github.ruediste1.i18n.label.Labeled;
import com.tutububu.Icon;
import com.tutububu.PageView;
import com.tutububu.RequiresAdmin;
import com.tutububu.SampleCanvas;
import com.tutububu.scrapers.Scraper;
import com.tutububu.scrapers.ScrapersAppCtrl;
import com.tutububu.scrapers.ScrapersAppCtrl.RunningScraper;

public class ScraperController extends ControllerMvc<ScraperController> {

    @Label("Scrapers")
    public static class View
            extends PageView<ScraperController, ScrapersAppCtrl> {

        @Override
        public void renderContent(SampleCanvas html) {
            html.h1().content(
                    label(ScrapersAppCtrl.class, x -> x.getRunningScrapers()));
            html.ul();
            for (RunningScraper scraper : data().getRunningScrapers()
                    .values()) {
                html.li().write(label(scraper.scraperClass))
                        .rButtonA(go().cancel(0))._li();
            }
            html._ul().bButtonA().HREF(go().index()).render(Glyphicon.refresh)
                    .content("Refresh");

            html.h1().content(label(ScrapersAppCtrl.class,
                    x -> x.getAvailableScrapers()));
            html.ul();
            for (Class<? extends Scraper> scraper : data()
                    .getAvailableScrapers()) {
                html.li().write(label(scraper)).rButtonA(go().start(scraper))
                        ._li();
            }
            html._ul();
        }

    }

    @Inject
    ScrapersAppCtrl appCtrl;

    @RequiresAdmin
    @UrlUnsigned
    @Label("Scrapers")
    @ActionPath("/admin")
    public ActionResult index() {
        return view(View.class, appCtrl);
    }

    @RequiresAdmin
    @Labeled
    @Icon(Glyphicon.play)
    public ActionResult start(Class<? extends Scraper> cls) {
        appCtrl.startScraper(cls);
        return redirect(go().index());
    }

    @RequiresAdmin
    @Labeled
    @Icon(Glyphicon.stop)
    public ActionResult cancel(int scraperId) {
        appCtrl.cancel(scraperId);
        return redirect(go().index());
    }
}
