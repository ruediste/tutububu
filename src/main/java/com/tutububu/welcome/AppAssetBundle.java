package com.tutububu.welcome;

import javax.inject.Inject;

import com.github.ruediste.rise.core.web.assetPipeline.Asset;
import com.github.ruediste.rise.core.web.assetPipeline.AssetBundle;
import com.github.ruediste.rise.core.web.assetPipeline.AssetBundleOutput;
import com.github.ruediste.rise.core.web.assetPipeline.AssetGroup;
import com.github.ruediste.rise.core.web.assetPipeline.AssetPipelineHelper;
import com.github.ruediste.rise.core.web.assetPipeline.CssProcessor;
import com.github.ruediste.rise.core.web.assetPipeline.DefaultAssetTypes;

public class AppAssetBundle extends AssetBundle {

    AssetBundleOutput out = new AssetBundleOutput(this);
    AssetGroup index;

    @Inject
    CssProcessor css;

    @Inject
    AssetPipelineHelper helper;

    @Override
    protected void initialize() {
        webJar("jquery", "jquery.js").merge(webJar("angularjs", "angular.js"))
                .merge(webJar("angular-ui-router", "angular-ui-router.js"))
                .insertMinInProd()
                .merge(locations("app/app.js", "app/app.scss",
                        "app/home/home.controller.js", "app/home/home.js",
                        "app/home/home.scss", "app/main/main.controller.js",
                        "app/main/main.js", "app/main/main.scss"))
                .load().process().select(DefaultAssetTypes.CSS)
                .split(g -> g.map(css.process("{qname}.{extT}", a -> a)))
                .join(locations("app/home/home.html", "app/main/main.html")
                        .load())
                .name("{qname}.{extT}").removePathInfoPrefix("assets/")
                .send(out);
        StringBuilder jsLinks = new StringBuilder();
        for (Asset a : out.assets) {
            if (a.getAssetType() == DefaultAssetTypes.JS) {
                jsLinks.append(
                        "<script src=\"" + helper.getUrl(a) + "\"></script>");
            }
        }
        StringBuilder cssLinks = new StringBuilder();
        for (Asset a : out.assets) {
            if (a.getAssetType() == DefaultAssetTypes.CSS) {
                cssLinks.append(
                        "<link rel=\"stylesheet\" type=\"text/css\" href=\""
                                + helper.getUrl(a) + "\">");
            }
        }
        index = locations("index.html").load()
                .replace("<!-- rise:js -->", jsLinks.toString())
                .replace("<!-- rise:css -->", cssLinks.toString());
    }
}
