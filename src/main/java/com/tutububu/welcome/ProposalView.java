package com.tutububu.welcome;

import java.util.Map;

import javax.inject.Inject;

import com.github.ruediste.rendersnakeXT.canvas.Glyphicon;
import com.github.ruediste.rise.integration.RisePageTemplate;
import com.github.ruediste.rise.integration.RisePageTemplate.RisePageTemplateParameters;
import com.github.ruediste1.i18n.label.Label;
import com.github.ruediste1.i18n.label.Labeled;
import com.google.common.base.Strings;
import com.tutububu.SampleCanvas;
import com.tutububu.TuTuBuBuAssetBundle;
import com.tutububu.ViewMvc;
import com.tutububu.activities.Activity;
import com.tutububu.activities.PropertyConfiguration;
import com.tutububu.activities.PropertyConfigurationCache;

@Labeled
@Label("Proposal")
public class ProposalView extends ViewMvc<ProposalController, Activity> {

    @Inject
    RisePageTemplate<SampleCanvas> risePageTemplate;

    @Inject
    PropertyConfigurationCache cache;

    @Inject
    TuTuBuBuAssetBundle bundle;

    @Override
    protected void render(SampleCanvas html) {
        risePageTemplate.renderOn(html,
                new RisePageTemplateParameters<SampleCanvas>() {

                    @Override
                    protected void renderJsLinks(SampleCanvas html) {
                        html.rJsLinks(bundle.out);
                        html.script()
                                .write("(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){\n"
                                        + "(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n"
                                        + "m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n"
                                        + "})(window,document,'script','//www.google-analytics.com/analytics.js','ga');\n"
                                        + "\n"
                                        + "ga('create', 'UA-68415903-1', 'auto');\n"
                                        + "$('#viewOriginPage').click(function(event){\n"
                                        + "event.preventDefault();"
                                        + "ga('send','event','Proposal iphone','viewOriginPage',{"
                                        + "hitCallback: function() {"
                                        + "  window.location.href=$('#viewOriginPage').attr('href');"
                                        + "}})});\n")
                                ._script();
                    }

                    @Override
                    protected void renderHead(SampleCanvas html) {
                        html.title().content("TuTuBuBu");
                    }

                    @Override
                    protected void renderCssLinks(SampleCanvas html) {
                        html.rCssLinks(bundle.out);
                    }

                    @Override
                    protected void renderBody(SampleCanvas html) {
                        //@formatter:off
                        html.div().ID("frontPage")
                            .img().SRC(data().getImgUrl())
                            .div().CLASS("properties")
                                .h1().content(data().getTitle())
                                .ul();
        
                                    Map<String, String> properties = data().getProperties();
                                    for (PropertyConfiguration config: cache.getConfigs()){
                                        String value = properties.get(config.getPropertyName());
                                        if (!Strings.isNullOrEmpty(value)){
                                            html.li().content(config.getLabel()+": "+value);
                                        }
                                        
                                    }
                               
                                html._ul()
                                 .bRow().BtextCenter().CLASS("buttonRow")
                                     .bCol(x->x.xs(6))
                                         .a().HREF(go().index()).render(Glyphicon.remove_circle)._a()
                                     ._bCol()
                                     .bCol(x->x.xs(6)).BcenterBlock()
                             .a().BcenterBlock().ID("viewOriginPage").HREF(data().getUrl()).render(Glyphicon.circle_arrow_right)._a()
                                     ._bCol()
                                 ._bRow()
                             ._div()
                        ._div();
                
            }
        });

    }
}
