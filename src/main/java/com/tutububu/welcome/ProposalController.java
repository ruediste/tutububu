package com.tutububu.welcome;

import javax.inject.Inject;

import org.slf4j.Logger;

import com.github.ruediste.rise.api.ControllerMvc;
import com.github.ruediste.rise.core.ActionResult;
import com.github.ruediste.rise.core.CoreRequestInfo;
import com.github.ruediste.rise.core.security.urlSigning.UrlUnsigned;
import com.github.ruediste.rise.core.web.ActionPath;
import com.github.ruediste.rise.core.web.ClasspathResourceRenderResultFactory;
import com.github.ruediste.rise.core.web.ContentRenderResult;
import com.github.ruediste.rise.core.web.assetPipeline.AssetBundle;
import com.github.ruediste.rise.core.web.assetPipeline.AssetBundleOutput;
import com.github.ruediste1.i18n.label.Label;
import com.tutububu.PageView;
import com.tutububu.SampleCanvas;
import com.tutububu.activities.ActivityRepository;

public class ProposalController extends ControllerMvc<ProposalController> {

    @Inject
    Logger log;

    @Inject
    ActivityRepository repo;

    @Inject
    ClasspathResourceRenderResultFactory factory;

    @Inject
    CoreRequestInfo info;

    @Inject
    AppAssetBundle appAssetBundle;

    @UrlUnsigned
    @Label("Home")
    @ActionPath(value = "/", primary = true)
    public ActionResult index() {
        String agent = info.getServletRequest().getHeader("User-Agent");
        if (agent != null && agent.contains("iPhone"))
            return view(ProposalView.class, repo.getRandomActivity());
        else
            return new ContentRenderResult(appAssetBundle.index);
    }

    @Label("Facebook")
    public static class FacebookView
            extends PageView<ProposalController, String> {
        public static class Bundle extends AssetBundle {
            AssetBundleOutput out = new AssetBundleOutput(this);

            @Override
            protected void initialize() {
                locations("/assets/facebook.js").load().send(out);
            }

        }

        @Inject
        Bundle bundle;

        @Override
        public void renderAdditionalJsLinks(SampleCanvas html) {
            html.rJsLinks(bundle.out);
        }

        @Override
        public void addBodyAttributes(SampleCanvas canvas) {
            canvas.DATA("login-url",
                    url(go(FaceBookLoginController.class).login()));
        }

        @Override
        public void renderContent(SampleCanvas html) {
            html.tag("fb:login-button")
                    .addAttribute("scope", "public_profile,email")
                    .addAttribute("onlogin", "checkLoginState();").close().div()
                    .ID("status")._div();
        }

    }

    @UrlUnsigned
    @Label("Facebook")
    @ActionPath("/fb")
    public ActionResult facebook() {
        return view(FacebookView.class, "");
    }

}
