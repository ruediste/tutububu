package com.tutububu.welcome;

import javax.inject.Inject;

import org.slf4j.Logger;

import com.github.ruediste.rise.api.ControllerMvc;
import com.github.ruediste.rise.core.ActionResult;
import com.github.ruediste.rise.core.CoreRequestInfo;
import com.github.ruediste.rise.core.httpRequest.HttpRequest;
import com.github.ruediste.rise.core.persistence.Updating;
import com.github.ruediste.rise.core.security.urlSigning.UrlUnsigned;
import com.github.ruediste.rise.core.security.web.FacebookLoginHelper;
import com.github.ruediste.rise.core.security.web.FacebookLoginHelper.SignedRequest;
import com.tutububu.user.UserScrapingAppCtrl;
import com.tutububu.user.UserService;

public class FaceBookLoginController
        extends ControllerMvc<FaceBookLoginController> {

    @Inject
    Logger log;

    @Inject
    CoreRequestInfo info;

    @Inject
    FacebookLoginHelper helper;

    @Inject
    UserService service;
    @Inject
    UserScrapingAppCtrl appCtrl;

    @UrlUnsigned
    @Updating
    public ActionResult login() {
        HttpRequest req = info.getRequest();
        String signedRequest = req.getParameter("signedRequest");
        String authenticationToken = req.getParameter("authenticationToken");
        SignedRequest parsedRequest = helper.parseRequest(signedRequest,
                System.getProperty("tutububu.facebook.appsecret"));

        service.processLogin(signedRequest);
        appCtrl.queue(parsedRequest, authenticationToken);

        return redirect(go(ProposalController.class).index());
    }
}
