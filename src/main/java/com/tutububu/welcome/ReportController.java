package com.tutububu.welcome;

import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import javax.inject.Inject;

import com.github.ruediste.rise.api.ControllerMvc;
import com.github.ruediste.rise.core.ActionResult;
import com.github.ruediste.rise.core.web.ContentRenderResult;
import com.github.ruediste1.i18n.label.Label;
import com.github.ruediste1.i18n.label.Labeled;
import com.tutububu.PageView;
import com.tutububu.RequiresAdmin;
import com.tutububu.SampleCanvas;
import com.tutububu.activities.ActivityRepository;

public class ReportController extends ControllerMvc<ReportController> {

    @Label("Reports")
    public static class IndexView extends PageView<ReportController, String> {

        @Override
        public void renderContent(SampleCanvas html) {
            html.rButtonA(go().downloadLocations());
        }

    }

    @Label("Reports")
    @RequiresAdmin
    public ActionResult index() {
        return view(IndexView.class, "");
    }

    @Inject
    ActivityRepository repo;

    @RequiresAdmin
    @Labeled
    public ActionResult downloadLocations() {
        StringWriter out = new StringWriter();
        repo.getAllActivities().stream()
                .flatMap(a -> a.getProperties().entrySet().stream())
                .filter(p -> p.getKey().equals("Ort"))
                .forEach(p -> out.write(p.getValue() + "\n"));
        try {
            return new ContentRenderResult(out.toString().getBytes("UTF-8"),
                    "application/octet-stream; charset=UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
