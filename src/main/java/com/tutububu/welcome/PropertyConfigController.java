package com.tutububu.welcome;

import javax.inject.Inject;

import com.github.ruediste.rise.api.ControllerMvc;
import com.github.ruediste.rise.core.ActionResult;
import com.github.ruediste1.i18n.label.Label;
import com.github.ruediste1.i18n.label.Labeled;
import com.tutububu.PageView;
import com.tutububu.RequiresAdmin;
import com.tutububu.SampleCanvas;
import com.tutububu.activities.PropertyConfiguration;
import com.tutububu.activities.PropertyConfigurationCache;
import com.tutububu.crud.CrudController;

public class PropertyConfigController
        extends ControllerMvc<PropertyConfigController> {

    @Labeled
    public static class View
            extends PageView<PropertyConfigController, String> {

        @Override
        public void renderContent(SampleCanvas html) {
            html.rButtonA(go().loadPropertyConfigs());
            html.rButtonA(go().browsePropertyConfigs());

        }
    }

    @Inject
    PropertyConfigurationCache cache;

    @RequiresAdmin
    @Labeled
    public ActionResult loadPropertyConfigs() {
        cache.loadConfig();
        return redirect(go().index());
    }

    @RequiresAdmin
    @Label("PropertyConfig")
    public ActionResult index() {
        return view(View.class, "");
    }

    @RequiresAdmin
    @Labeled
    public ActionResult browsePropertyConfigs() {
        return redirect(go(CrudController.class)
                .browse(PropertyConfiguration.class, null));
    }
}
