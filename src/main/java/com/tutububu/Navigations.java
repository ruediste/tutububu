package com.tutububu;

import javax.inject.Singleton;

import com.github.ruediste.rise.core.navigation.Navigation;
import com.github.ruediste.rise.core.navigation.NavigationsContainer;
import com.github.ruediste1.i18n.label.MembersLabeled;
import com.tutububu.crud.CrudController;
import com.tutububu.welcome.PropertyConfigController;
import com.tutububu.welcome.ProposalController;
import com.tutububu.welcome.ReportController;
import com.tutububu.welcome.ScraperController;

@Singleton
public class Navigations extends NavigationsContainer {

    @MembersLabeled
    private enum GroupLabels {
        A
    }

    Navigation topNavigation;

    @Override
    protected void initializeImpl() {
        topNavigation = build().add(go(ProposalController.class).index())
                .add(go(ScraperController.class).index())
                .add(go(CrudController.class).showActivities())
                .add(go(PropertyConfigController.class).index())
                .add(go(ProposalController.class).facebook())
                .add(go(ReportController.class).index()).getResult();
    }
}
