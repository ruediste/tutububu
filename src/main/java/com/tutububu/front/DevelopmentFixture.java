package com.tutububu.front;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.tutububu.activities.PropertyConfiguration;

public class DevelopmentFixture implements Runnable {

    @Inject
    EntityManager em;

    @Override
    public void run() {
        createConfig(0, "Künstler", "performer");
        createConfig(1, "Stil", "eventtype");
        createConfig(2, "Ort", "locality");
        createConfig(3, "Datum", "date");
    }

    private void createConfig(int ordinal, String label, String propertyName) {
        PropertyConfiguration config = new PropertyConfiguration();
        config.setOrdinal(ordinal);
        config.setLabel(label);
        config.setPropertyName(propertyName);
        em.persist(config);
    }

}
