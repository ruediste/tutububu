package com.tutububu.front;

import com.github.ruediste.rise.integration.RiseServer;

public class TuTuBuBuMain {

    public static void main(String[] args) throws Exception {

        // NodeBuilder builder = NodeBuilder.nodeBuilder().local(true);
        // builder.settings().put("path.home", "./elastic");
        // Node node = builder.build();

        RiseServer app = new RiseServer();
        app.start(TuTuBuBuFrontServlet.class, 5000);
        app.join();
    }
}
