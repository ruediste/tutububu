package com.tutububu.web;

import com.github.ruediste.rise.core.web.assetDir.AssetDir;

public class AssetsAssetDir extends AssetDir {

    @Override
    protected String getLocation() {
        return "/assets/assets";
    }

    @Override
    protected String getName() {
        return "/assets";
    }

}
