package com.tutububu.web;

import com.github.ruediste.rise.core.web.assetPipeline.AssetBundle;
import com.github.ruediste.rise.core.web.assetPipeline.AssetBundleOutput;

public class WebAssetBundle extends AssetBundle {

    AssetBundleOutput out = new AssetBundleOutput(this);

    @Override
    protected void initialize() {
        locations("/assets/index.html").load().forkJoin(x -> x.name("/home"))
                .send(out);
        locations("/assets/favicon.ico", "/assets/robots.txt").load()
                .name("{name}.{ext}").send(out);
    }
}
