package com.tutububu.user;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;

import org.slf4j.Logger;

import com.github.ruediste.rise.core.persistence.TransactionTemplate;
import com.github.ruediste.rise.core.security.web.FacebookLoginHelper.SignedRequest;
import com.tutububu.activities.EmHelper;

@Singleton
public class UserService {

    @Inject
    Logger log;
    @Inject
    EntityManager em;

    @Inject
    EmHelper emh;

    @Inject
    TransactionTemplate trx;

    public void processLogin(String facebookUserId) {
        log.info("processing user login: {}", facebookUserId);
        User user = em.find(User.class, facebookUserId);
        if (user == null) {
            user = new User();
            user.setFacebookId(facebookUserId);
            em.persist(user);
            log.info("created new user {}", facebookUserId);
        }
    }

    public void scrapeUser(SignedRequest request, String accessToken) {
        trx.updating().execute(() -> {
            User user = em.find(User.class, request.userId);
        });
    }
}
