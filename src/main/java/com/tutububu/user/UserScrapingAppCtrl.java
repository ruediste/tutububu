package com.tutububu.user;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import javax.annotation.Signed;
import javax.inject.Inject;
import javax.inject.Singleton;

import com.github.ruediste.rise.core.security.web.FacebookLoginHelper.SignedRequest;

@Singleton
public class UserScrapingAppCtrl {

    BlockingQueue<Signed> requests = new LinkedBlockingQueue<>();

    ExecutorService executor = Executors.newFixedThreadPool(4);

    @Inject
    UserService service;

    @Inject
    org.slf4j.Logger log;

    public void queue(SignedRequest request, String accessToken) {
        log.info("Queuing user {} for scraping", request.userId);
        executor.submit(() -> service.scrapeUser(request, accessToken));
    }
}
