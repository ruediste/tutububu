package com.tutububu.api.v1;

import java.util.Map;

import javax.inject.Inject;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.github.ruediste.rise.api.ControllerMvc;
import com.github.ruediste.rise.core.ActionResult;
import com.github.ruediste.rise.core.security.urlSigning.UrlUnsigned;
import com.github.ruediste.rise.core.web.JsonRenderResultFactory;
import com.tutububu.activities.Activity;
import com.tutububu.activities.ActivityRepository;
import com.tutububu.activities.PropertyConfiguration;
import com.tutububu.activities.PropertyConfigurationCache;

public class ActivitiesController extends ControllerMvc<ActivitiesController> {

    @Inject
    JsonRenderResultFactory factory;

    @Inject
    ActivityRepository repo;

    @Inject
    PropertyConfigurationCache cache;

    @UrlUnsigned
    public ActionResult index() {
        Activity activity = repo.getRandomActivity();
        JSONObject obj = new JSONObject();
        obj.put("title", activity.getTitle());
        obj.put("url", activity.getUrl());
        obj.put("imgUrl", activity.getImgUrl());
        JSONArray properties = new JSONArray();
        Map<String, String> props = activity.getProperties();
        for (PropertyConfiguration config : cache.getConfigs()) {
            String value = props.get(config.getPropertyName());
            if (value != null) {
                JSONObject propObj = new JSONObject();
                propObj.put("label", config.getLabel());
                propObj.put("text", value);
                properties.add(propObj);

            }
        }
        obj.put("properties", properties);
        return factory.jsonRenderResult(obj);
    }

}
