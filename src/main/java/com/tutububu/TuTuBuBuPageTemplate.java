package com.tutububu;

import javax.inject.Inject;

import com.github.ruediste.rise.core.navigation.NavigationRenderer;
import com.github.ruediste.rise.integration.PageTemplateBase;
import com.github.ruediste.rise.integration.RisePageTemplate;
import com.github.ruediste.rise.integration.RisePageTemplate.RisePageTemplateParameters;
import com.github.ruediste1.i18n.lString.LString;
import com.tutububu.welcome.ProposalController;

public class TuTuBuBuPageTemplate extends PageTemplateBase {

    @Inject
    TuTuBuBuAssetBundle bundle;

    @Inject
    RisePageTemplate<SampleCanvas> risePageTemplate;

    @Inject
    Navigations navigations;

    @Inject
    NavigationRenderer navRenderer;

    public void renderOn(SampleCanvas canvas,
            SamplePageTemplateParameters parameters) {
        // your drawing code
        // @formatter:off
        risePageTemplate.renderOn(canvas,
                new RisePageTemplateParameters<SampleCanvas>() {

                    @Override
                    protected void renderJsLinks(SampleCanvas html) {
                        html.rJsLinks(bundle.out);
                        parameters.renderAdditionalJsLinks(html);
                    }

                    @Override
                    protected void renderCssLinks(SampleCanvas html) {
                        html.rCssLinks(bundle.out);
                    }

                    @Override
                    protected void renderHead(SampleCanvas html) {
                        html.title().content(parameters.getTitle());
                    }

                    @Override
                    protected void addBodyAttributes(SampleCanvas canvas) {
                        parameters.addBodyAttributes(canvas);
                    }
                    @Override
                    protected void renderBody(SampleCanvas html) {
                        html.render(risePageTemplate.stageRibbon(false,x-> go(StageRibbonController.class).index(x)));
                        html.bNavbar("mainNav", opts->{}, ()->html.a().BNavbarBrand().HREF(go(ProposalController.class).index()).content("TuTuBuBu"), ()->{
                            navRenderer.renderNavItems(html, navigations.topNavigation)
                            ;
                        })
                        .bContainer_fluid()
                            .bRow()
                                .bCol(x->x.xs(12));
                                    parameters.renderContent(html);
                                html._bCol()
                            ._bRow()
                        ._bContainer_fluid();
                    }
                });
        // @formatter:on
    }

    public interface SamplePageTemplateParameters {
        LString getTitle();

        default void renderAdditionalJsLinks(SampleCanvas html) {
        }

        default void addBodyAttributes(SampleCanvas canvas) {
        }

        void renderContent(SampleCanvas html);
    }
}
