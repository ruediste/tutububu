package com.tutububu.crud;

import java.lang.annotation.Annotation;

import com.github.ruediste.rise.component.components.CComponentStack;
import com.github.ruediste.rise.component.components.CPage;
import com.github.ruediste.rise.component.tree.Component;
import com.github.ruediste.rise.core.ActionResult;
import com.github.ruediste.rise.crud.CrudControllerBase;
import com.github.ruediste1.i18n.label.Labeled;
import com.tutububu.RequiresAdmin;
import com.tutububu.ViewComponent;
import com.tutububu.activities.Activity;

public class CrudController extends CrudControllerBase {

    @Labeled
    public static class View extends ViewComponent<CrudController> {

        @Override
        protected Component createComponents() {
            return new CPage(label(this))
                    .add(new CComponentStack(toSubView(() -> controller.data(),
                            x -> x.getSubController())));
        }
    }

    @RequiresAdmin
    @Labeled
    public ActionResult showActivities() {
        return browse(Activity.class, null);
    }

    @RequiresAdmin
    @Override
    public ActionResult browse(Class<?> entityClass,
            Class<? extends Annotation> emQualifier) {
        return super.browse(entityClass, emQualifier);
    }
}
