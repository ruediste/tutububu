to use this, clone 


git clone https://github.com/ruediste/whatwg-html5-std-parser.git
cd whatwg-html5-std-parser
mvn install
cd ..

git clone https://github.com/ruediste/c3java.git
cd c3java
mvn install
cd ..

git clone https://github.com/ruediste/attached-properties-4j.git
cd attached-properties-4j
mvn install
cd ..

git clone https://github.com/ruediste/rendersnakeXT.git
cd rendersnakeXT
mvn install
cd ..

git clone https://github.com/ruediste/remote-junit.git
cd remote-junit
mvn install -DskipTests
cd ..

git clone https://github.com/ruediste/salta.git
cd salta
mvn install
cd ..

git clone https://github.com/ruediste/lambda-peg-parser.git
cd lambda-peg-parser
mvn install
cd ..

git clone https://github.com/ruediste/i18n.git
cd i18n
mvn install
cd ..

git clone https://github.com/ruediste/jabsaw.git
cd jabsaw
mvn install -DskipTests
cd ..

git clone https://github.com/ruediste/rise.git
cd rise
mvn install -DskipTests
cd ..

